#pragma once
typedef void (WINAPI *CallbackFun)(unsigned int adder,unsigned int pt);
extern "C" _declspec(dllexport) int __stdcall InitFiddler(int port, int callback);
extern "C" _declspec(dllexport) void __stdcall CloseFiddler();
extern "C" _declspec(dllexport) void __stdcall AutoStartFiddlerProxy(bool start);
extern "C" _declspec(dllexport) void __stdcall StartSSL(bool start);
extern "C" _declspec(dllexport) void __stdcall SetBase64(int* url);
extern "C" _declspec(dllexport) int __stdcall CreateRootCert();
extern "C" _declspec(dllexport) int __stdcall InstCert();
extern "C" _declspec(dllexport) bool __stdcall rootCertIsTrusted();
extern "C" _declspec(dllexport) bool __stdcall rootCertIsMachineTrusted();
extern "C" _declspec(dllexport) bool __stdcall rootCertExists();
extern "C" _declspec(dllexport) bool __stdcall removeCerts();

